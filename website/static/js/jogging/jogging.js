/**
 * Created by abomariam on 24/01/16.
 */

(function(){
    'use strict';

    angular
        .module('jogging',[
            'jogging.config',
            'jogging.authentication',
            'jogging.routes',
            'jogging.profiles',
            'jogging.entries'
        ])


    angular
        .module('jogging.config',[]);

    angular
        .module('jogging.routes',['ngRoute']);

    angular
        .module('jogging')
        .run(run);

    run.$inject = ['$http'];

    function run($http){
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();