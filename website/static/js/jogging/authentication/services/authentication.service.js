/**
 * Created by abomariam on 24/01/16.
 */

(function(){
    'use strict';

    angular
        .module('jogging.authentication.services')
        .factory('Authentication',Authentication)

    Authentication.$inject = ['$cookies','$http']

    function Authentication($cookies,$http){

        var Authentication = {
            register:register,
            login:login,
            logout:logout,
            setAuthenticatedAccount:setAuthenticatedAccount,
            unauthenticate:unauthenticate,
            isAuthenticated:isAuthenticated,
            getAuthenticatedAccount:getAuthenticatedAccount,
            activate:activate
        }

        return Authentication;

        function register(data){
            return $http.post(
                'api/v1/accounts/register/',
                data
            );

        }


        function login(data){
            return $http.post(
                'api/v1/auth/login/',
                data
            ).then(successFn,errorFn);

            function successFn(data){
                Authentication.setAuthenticatedAccount(data.data);
                window.location = '/+'+data.data.username+'/entries';
            }

            function errorFn(data){
                return data;
            }
        }

        function logout(){
            return $http.post('api/v1/auth/logout/').then(successFn,errorFn);

            function successFn(data){
                Authentication.unauthenticate();
                window.location = '/';
            }

            function errorFn(data){
                Materialize.toast(data.data, 8000);
            }
        }

        function setAuthenticatedAccount(account){
            $cookies.put('authenticatedAccount',JSON.stringify(account));
        }

        function unauthenticate(){
            $cookies.remove('authenticatedAccount');
        }

        function isAuthenticated(){
            return (!!$cookies.get('authenticatedAccount'));

        }

        function getAuthenticatedAccount(){
            if(!Authentication.isAuthenticated()){
                return;
            }
            return JSON.parse($cookies.get('authenticatedAccount'));
        }

        function activate(){
            return $http.get('api/v1/auth/login/').then(successFn,errorFn);

            function successFn(data){
                Authentication.setAuthenticatedAccount(data.data);
            }

            function errorFn(data){
                if (!!$cookies.get('authenticatedAccount'))
                    Authentication.unauthenticate();
            }
        }
    }
})();