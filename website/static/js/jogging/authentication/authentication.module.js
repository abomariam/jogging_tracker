/**
 * Created by abomariam on 24/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.authentication',[
            'jogging.authentication.controllers',
            'jogging.authentication.services'
        ]);

    angular
        .module('jogging.authentication.controllers',[]);

    angular
        .module('jogging.authentication.services',['ngCookies']);
})();
