/**
 * Created by abomariam on 24/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.authentication.controllers')
        .controller('NavbarController',NavbarController);

    NavbarController.$inject = ['$scope','Authentication'];


    function NavbarController($scope,Authentication){

        var vm = this;

        vm.logout = logout;

        activate();
        function activate(){
            Authentication.activate();
            $('.slider').slider();
        }
        function logout(){
            Authentication.logout();
        }
    }
})();