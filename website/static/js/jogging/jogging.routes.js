/**
 * Created by abomariam on 24/01/16.
 */

(function(){
    'user strict';

    angular
        .module('jogging.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider){
        $routeProvider
            .when('/', {
                controller: 'NavbarController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/home.html'
            })
            .when('/register', {
                controller: 'RegisterController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/authentication/register.html'
            }).when('/login',{
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/authentication/login.html'
            }).when('/+:username/profile',{
                controller: 'ProfileController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/profiles/profile.html'
            }).when('/+:username/edit',{
                controller: 'ProfileEditController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/profiles/edit.html'
            }).when('/+:username/entries',{
                controller: 'UserEntriesController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/entries/user_entries.html'
            }).when('/dashboard',{
                controller: 'AdminController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/profiles/user_list.html'
            })
            .otherwise('/');
    }
})();