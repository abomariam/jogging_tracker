/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.profiles.controllers')
        .controller('ProfileController',ProfileController);


    ProfileController.$inject = ['$location', '$routeParams', 'Profile', 'Authentication'];

    function ProfileController($location,$routeParams,Profile, Authentication){

        var vm = this;

        vm.user = undefined

        activate();

        function activate(){

            var username = $routeParams.username.substr(1);

            if (!Authentication.isAuthenticated() || Authentication.getAuthenticatedAccount().username != username){

                if (!(Authentication.getAuthenticatedAccount().roles.is_admin || Authentication.getAuthenticatedAccount().roles.is_manager)) {
                    $location.url('/');
                    Materialize.toast('You aren\'t authorized to view this profile', 800);
                    return;
                }
            }

            Profile.get(username).then(profileSuccessFn, profileErrorFn);

            function profileSuccessFn(data) {
              vm.user = data.data;
            }

            function profileErrorFn(data) {
                $location.url('/');
                Materialize.toast('That user does not exist.', 8000);
            }
        }
    }
})();