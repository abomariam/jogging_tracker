/**
 * Created by abomariam on 27/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.profiles.controllers')
        .controller('AdminController',AdminController);

    AdminController.$inject = ['$location','Authentication','Profile'];


    function AdminController($location,Authentication,Profile){
        var vm = this;

        vm.register = register;

        vm.authenticatedUser = Authentication.getAuthenticatedAccount();
        activate();

        function activate(){
            if (!Authentication.isAuthenticated()){
                if (!(Authentication.getAuthenticatedAccount().roles.is_admin || Authentication.getAuthenticatedAccount().roles.is_manager)) {
                    $location.url('/');
                    Materialize.toast('You aren\'t authorized to view this page', 4000);
                    return;
                }
            }

            Profile.list().then(successFn,errorFn);

            function successFn(data){
                vm.users = data.data;
            }
            function errorFn(data){
                for (var field in data.data){
                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)

                    }
                }
            }
        }



        function register(){
            Authentication.register(vm.form)
                .then(successFn,errorFn);

            function successFn(data){
                vm.users.push(data.data);
                vm.form = undefined
                $('#modal1').closeModal();
            }

            function errorFn(data){
                for (var field in data.data){

                    $('#'+field).addClass('invalid');

                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)

                    }
                }
            }


        }
    }
})();