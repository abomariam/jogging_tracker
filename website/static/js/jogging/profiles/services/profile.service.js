/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.profiles.services')
        .factory('Profile',Profile);

    Profile.$inject = ['$http','Authentication'];

    function Profile($http,Authentication){
        var Profile = {
            get:get,
            update:update,
            destroy:destroy,
            list:list
        }


        return Profile;

        function get(username){
            return $http.get('/api/v1/accounts/+'+username+'/');
        }

        function update(username,data){

            if (data.password && data.password.count() == 0)
                delete data.password;

            if (data.confirm_password && data.confirm_password.count() == 0)
                delete data.confirm_password;

            return $http.put('/api/v1/accounts/+'+username+'/', data);
        }

        function destroy(username){
            return $http.delete('/api/v1/accounts/+'+username+'/');


        }

        function list(){
            return $http.get('/api/v1/admin/accounts/');
        }
    }
})();