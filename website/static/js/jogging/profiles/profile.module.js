/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.profiles',[
            'jogging.profiles.controllers',
            'jogging.profiles.services'
            ]);

    angular
        .module('jogging.profiles.controllers',[]);

    angular
        .module('jogging.profiles.services',[]);
})();