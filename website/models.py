from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Roles(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,related_name='roles')
    is_admin = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)


class Entry(models.Model):
    date = models.DateField()
    distance = models.FloatField()
    time = models.DurationField()

    user = models.ForeignKey(User)

    @property
    def avg_speed(self):
        return round((self.distance  / self.time.total_seconds()) * 60 * 60,2)

    def __unicode__(self):
        return  ' - '.join((str(self.user),str(self.date),str(self.distance)+' Km',str(self.time)))


    class Meta:
        verbose_name_plural = 'Entries'