from django.shortcuts import render
from rest_framework import viewsets,permissions,views,generics,status
from rest_framework.response import Response
from django.contrib.auth.models import User
from serializers import UserSerializer,EntrySerializer
from website.models import Entry
from copy import deepcopy
from django.contrib.auth import authenticate, login, logout
from api.permissions import IsAccountOwner,IsAdmin,IsManagerOrAdmin, IsEntryOwnerOrAdmin,IsAccountOwnerOrManagerOrAdmin
import json
# Create your views here.


# class UserViewSet(viewsets.ModelViewSet):
#     lookup_field = 'username'
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#
#     def get_permissions(self):
#         if self.request.method == ''

class RegisterView(generics.CreateAPIView):
    serializer_class = UserSerializer

    def post(self,request, format=None):
        data = deepcopy(request.data)
        if request.user.is_authenticated():
            if not request.user.roles.is_admin:
                if 'roles' in data:
                    data.get('roles').pop('is_admin',None)

            if not request.user.roles.is_admin and not request.user.roles.is_manager:
                data.pop('roles',None)
        else:
            data.pop('roles',None)
        serialized = self.serializer_class(data=request.data)
        if serialized.is_valid():
            user = serialized.save()
            return Response(self.serializer_class(user).data,status=status.HTTP_201_CREATED)

        return Response(serialized.errors,status=status.HTTP_400_BAD_REQUEST)


class UpdateProfileView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    lookup_field = 'username'
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,IsAccountOwnerOrManagerOrAdmin)

    def put(self, request, *args, **kwargs):
        request.data.pop('username',None)

        if not request.user.roles.is_admin:
            request.data.get('roles').pop('is_admin')

        if not request.user.roles.is_admin and not request.user.roles.is_manager:
            request.data.pop('roles',None)


        return self.partial_update(request, *args, **kwargs)


class AdminUsersView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    lookup_field = 'username'
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,IsManagerOrAdmin)

    def put(self, request, *args, **kwargs):
        request.data.pop('username',None)

        return self.partial_update(request, *args, **kwargs)

class LoginView(views.APIView):
    def get(self,request, format=None):
        if request.user.is_authenticated():
            return Response(UserSerializer(request.user).data)
        return Response({},status.HTTP_401_UNAUTHORIZED)


    def post(self,request, format=None):

        username = request.data.get('username',None)
        password = request.data.get('password',None)

        account = authenticate(username=username,password=password)

        if account is not None:
            if account.is_active:
                login(request,account)
                serialized = UserSerializer(account)

                return Response(serialized.data)
            else:
                return Response({'error':['This account has been disabled']},status=status.HTTP_401_UNAUTHORIZED)

        return Response({'error':['Invalid Username or Password']},status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self,request,format=None):
        logout(request)

        return Response({},status=status.HTTP_204_NO_CONTENT)



class EntryView(viewsets.ModelViewSet):
    queryset = Entry.objects.order_by('-date')
    serializer_class = EntrySerializer

    permission_classes = (permissions.IsAuthenticated,IsEntryOwnerOrAdmin)

    def list(self, request, *args, **kwargs):
        queryset = self.queryset.filter(user__username=kwargs['username'])
        serialized = self.serializer_class(queryset,many=True)

        return Response(serialized.data)

    def create(self, request, *args, **kwargs):
        user = User.objects.get(username=kwargs['username'])

        if request.user.roles.is_admin or request.user == user:
            request.data['username'] = user.username
        else:
            return Response({'Error':'You aren\'t authorized to do that'})

        return super(EntryView,self).create(request,*args,**kwargs)