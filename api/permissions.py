from rest_framework import permissions


class IsAccountOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return account == request.user
        return False


class IsAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return request.user.roles.is_admin
        return False

class IsManagerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return request.user.roles.is_admin or request.user.roles.is_manager
        return False

class IsEntryOwnerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            return request.user.roles.is_admin or request.user == obj.user
        return False

class IsAccountOwnerOrManagerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            return request.user.roles.is_admin or request.user.roles.is_manager or request.user == obj.user