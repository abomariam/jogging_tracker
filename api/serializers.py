from rest_framework import serializers
from django.contrib.auth.models import User
from website.models import Roles,Entry


class RolesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Roles
        fields = ('is_admin','is_manager')

class UserSerializer(serializers.ModelSerializer):
    roles = RolesSerializer(required=False)
    password = serializers.CharField(write_only=True, required=False)
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = ('id','email', 'username', 'first_name', 'last_name','password','confirm_password',
                  'roles')

    def create(self, validated_data):
        password = validated_data.pop('password',None)
        confirm_password = validated_data.pop('confirm_password',None)
        roles = validated_data.pop('roles',None)

        if not password:
            raise serializers.ValidationError({'password':['You must enter a valid password']})

        if not confirm_password:
            raise serializers.ValidationError({'confirm_password':['You must confirm the password']})


        if password == confirm_password:
            user = User.objects.create(**validated_data)
            user.set_password(password)
        else:
            raise serializers.ValidationError({'confirm_password':['The password and confirmation didn\'t match']})

        if roles:
            user.roles = Roles(
                is_admin=roles.get('is_admin',False),
                is_manager = roles.get('is_manager',False)
            )
            user.roles.save()
        else:
            user.roles = Roles()
            user.roles.save()

        user.save()
        return user



    def update(self, instance, validated_data):
        email = validated_data.get('email',None)
        first_name = validated_data.get('first_name',None)
        last_name = validated_data.get('last_name',None)
        password = validated_data.get('password',None)
        confirm_password = validated_data.get('confirm_password',None)
        roles = validated_data.get('roles',None)


        instance.email = email
        instance.first_name = first_name
        instance.last_name = last_name

        if password :
            if confirm_password and password == confirm_password:
                instance.set_password(password)
            else:
                raise serializers.ValidationError({'confirm_password':['The password and confirmation didn\'t match']})

        if roles:
            try:
                instance.roles
            except:
                instance.roles = Roles()

            instance.roles.is_admin=roles.get('is_admin',instance.roles.is_admin)
            instance.roles.is_manager = roles.get('is_manager',instance.roles.is_manager)

            instance.roles.save()

        instance.save()

        return instance

class EntrySerializer(serializers.ModelSerializer):

    def get_username(self,obj):
        return obj.user.username

    username = serializers.SerializerMethodField()

    class Meta:
        model = Entry
        fields = ('id','date','distance','time','avg_speed','username')
        read_only_fields = ('id','avg_speed')


    def create(self, validated_data):
        user = User.objects.get(username=self.initial_data['username'])

        validated_data['user_id'] = user.id

        return super(EntrySerializer,self).create(validated_data)